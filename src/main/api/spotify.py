import os
from flask import Blueprint, jsonify

spotifyClientId = os.environ.get('SPOTIFY_CLIENT_ID')
spotifyClientSecret = os.environ.get('SPOTIFY_CLIENT_SECRET')
spotifyRedirectUri = os.environ.get('SPOTIFY_REDIRECT_URI')

spotify_api = Blueprint("spotify_api", __name__)


@spotify_api.route('/credentials', methods=['GET'])
def get_redirect_uri():
    return jsonify(
        cliendId=spotifyClientId,
        clientSecret=spotifyClientSecret,
        redirectUrl=spotifyRedirectUri
    )
