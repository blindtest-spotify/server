# server/main/app.py
from flask import Flask
from api.spotify import spotify_api
app = Flask(__name__)

app.register_blueprint(spotify_api, url_prefix="/spotify")

@app.route('/')
def hello_world():
    return 'Blind test server is working !'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')